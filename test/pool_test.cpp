#include <catch.hpp>

#include <chrono>
#include <iostream>
#include <thread>

#include "pool.h"

struct Test {
  int i;
  char c;
};

TEST_CASE("Pool initializes properly", "[pool][constructor]") {
  sph::pool<Test> p1(5);
  sph::pool<Test> p2 {{15, 'f'}};
  sph::pool<Test> p3(std::vector<Test>(13));

  REQUIRE(p1.capacity() == 5);
  REQUIRE(p2.capacity() == 1);
  REQUIRE(p3.capacity() == 13);
}

TEST_CASE("Pool returns a pointer to a free element",
          "[pool][PointerT][initializer_list]") {
  Test t = {9, 'c'};
  sph::pool<Test> sut {t};

  auto p = sut.get();

  REQUIRE(p != nullptr);
  REQUIRE(p->i == 9);
  REQUIRE(p->c == 'c');
}

TEST_CASE("hasFreeElements() returns correct value",
          "[pool][bool][freeElements]") {
  sph::pool<Test> sut(2);

  REQUIRE(sut.hasFreeElements());

  auto p1 = sut.get();

  REQUIRE(sut.hasFreeElements());

  {
    auto p2 = sut.get();

    REQUIRE(!sut.hasFreeElements());
  }

  REQUIRE(sut.hasFreeElements());
}

SCENARIO("Object pool behaves properly in a multithreaded environment") {
  GIVEN("A thread-safe object pool") {
    using namespace std::chrono_literals;

    sph::pool<Test> sut(3);

    WHEN("A thread returns an object") {
      Test* ptr;

      std::thread t1([&]() {
        auto p1 = sut.get();
        std::this_thread::sleep_for(1s);
      });

      // to simulate a race condition
      // t2 and t3 both sleep for approximately the same amount of time
      // (obviously, for the purposes of the test, t2 will join first)
      std::thread t2([&]() {
        auto p2 = sut.get();

        // save the address of the received object for later verification
        ptr = p2.get();

        std::this_thread::sleep_for(437ms);
      });

      std::thread t3([&]() {
        auto p3 = sut.get();
        std::this_thread::sleep_for(600ms);
      });

      THEN("Another can acquire said object immediately afterwards") {
        std::thread t4([&]() {
          // sleep for a bit longer than t2
          std::this_thread::sleep_for(500ms);
          auto p4 = sut.get();

          // The returned object should be the same that t2 received
          REQUIRE(ptr == p4.get());
        });

        t4.join();
      }

      t1.join();
      t2.join();
      t3.join();
    }
  }
}
