#pragma once

#include <algorithm>
#include <functional>
#include <initializer_list>
#include <memory>
#include <mutex>
#include <vector>

namespace sph {

/**
 * A generic object pool
 *
 * @type T - the type of the objects inside the pool
 * @type BufferT - the type of the underlying object container. Defaults to std::vector
 * @type PointerT - the return type of the get() method. Defaults to std::unique_ptr
 *     Note: In case you supply your own pointer, you need to ensure it is
 *     a generic of type <T, F>, where F is the function, called upon the destruction of the pointer
 **/
template <typename T,
          typename BufferT = std::vector<T>,
          typename PointerT = std::unique_ptr<T, std::function<void(T*)>>>
class pool {
 public:
  // Initializes a pool with N default-constructed objects
  explicit pool(size_t size) : pool(BufferT(size)) {}

  // Initializes a pool from the initializer list
  explicit pool(std::initializer_list<T> l)
      : _max(l.size()), _free_index(0), _free(l.size(), true), _buf(l) {}

  explicit pool(BufferT const& buf)
      : _max(buf.size()), _free_index(0), _free(buf.size(), true), _buf(buf) {}

  explicit pool(BufferT const&& buf) noexcept
      : _max(buf.size()),
        _free_index(0),
        _free(buf.size(), true),
        _buf(std::move(buf)){}

  // Returns the first currently free object from the pool
  // Does not check whether there are free elements
  // Blocks until complete
  // @return a smart pointer to a free object
  [[nodiscard]] PointerT get() {
    auto const current_i = getFreeIndex();

    std::lock_guard<std::mutex> lck(_m);
    _free[current_i] = false;

    // Constructs a smart pointer to a free object and sets up the object's return
    // to the pool upon the pointer's destruction
    return PointerT(&_buf[current_i], [&, current_i](T*) {
      std::lock_guard<std::mutex> lck(_m);

      if (this->_free_index > current_i) {
        this->_free_index = current_i;
      }

      _free[current_i] = true;
    });
  }

  // @return whether the pool has any free elements left
  [[nodiscard]] bool hasFreeElements() noexcept { return getFreeIndex() != _max; }

  // @return the overall capacity of the pool
  [[nodiscard]] constexpr size_t capacity() const noexcept { return _max; }

 private:
  // @return the index of the first currently free object
  [[nodiscard]] size_t getFreeIndex() noexcept {
    std::lock_guard<std::mutex> lck(_m);

    return std::distance(_free.begin(), std::find(_free.begin() + _free_index,
                                                  _free.end(), true));
  }

  size_t _max;
  size_t _free_index;
  std::vector<bool> _free;
  BufferT _buf;
  std::mutex _m;
};

}  // namespace sph
